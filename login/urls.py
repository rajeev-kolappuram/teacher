from django.urls import path
from . import views

urlpatterns = [
    path('', views.loginView, name='login'),
    path('login-action', views.loginAction, name='login-action'),
    path('logout', views.logoutAction, name='logout'),
]
