from typing import cast
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import logout, authenticate, login


# Create your views here.
# Admin's login 
def loginView(request):
    return render(request, 'login.html')


def loginAction(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        
        if user is not None:
            login(request, user)
            return JsonResponse({
                'status': True,
                'message': 'Login Successfull.'
            })
        else:
            return JsonResponse({
                'status': False,
                'message': 'Username or password is incorrect.'
            })


def logoutAction(request):
    logout(request)

    return JsonResponse({
        'status': True,
        'message': 'Log out successfully.'
    })
