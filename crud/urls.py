from django.urls import path
from . import views

urlpatterns = [
    path('list', views.listView, name='teachers-list'),
    path('add', views.add, name='teachers-add'),
    path('get-teachers-list', views.getTeachersList, name='get-teachers-list'),
    path('detail-teacher-view/<int:id>/', views.detailsView, name='detail-teacher-view'),
    path('edit/<int:id>/', views.edit, name='teachers-edit'),
    path('teacher-details', views.teacherDetails, name='teacher-details'),
    path('teacher-delete', views.teacherDelete, name='teacher-delete'),
]
