from django.db import models
from multiselectfield import MultiSelectField


# Create your models here.
class Teachers(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    profile_picture = models.ImageField(upload_to='profile', blank=True)
    email = models.EmailField(max_length=250, unique=True)
    phone_number = models.IntegerField()
    room_number = models.IntegerField()
    subject_choices = (
        (1, 'Physics'),
        (2, 'Computer Science'),
        (3, 'History'),
        (4, 'Mathematics'),
        (4, 'Maths'),
        (5, 'Biology'),
        (6, 'Chemistry'),
        (7, 'English'),
        (8, 'Arabic'),
        (9, 'Geography')
    )
    subjects = MultiSelectField(choices=subject_choices, max_choices=5)
