from django.shortcuts import render
from django.http import JsonResponse
from django.core import serializers
from django.db.models import Q
from django.shortcuts import redirect

from .forms import TeacherForm
from .models import Teachers

import json


# Create your views here.
def listView(request):
    if request.user.is_authenticated:
        return render(request, 'list.html')
    else:
        return redirect('/')


def getTeachersList(request):
    teachers = {}
    if request.method == 'POST':
        search = request.POST['search']
        teachers = list(Teachers.objects.filter(Q(last_name__contains=search) | Q(first_name__contains=search)).values())
        return JsonResponse(teachers, safe=False)
    else:
        teachers = list(Teachers.objects.values())
        return JsonResponse(teachers, safe=False)


def add(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            teachers = TeacherForm(request.POST, request.FILES)

            if teachers.is_valid():
                teachers.save()
                return redirect('/teachers/list')
        form = TeacherForm
        return render(request, 'add_teacher.html', { 'form': form })
    else:
        return redirect('/')


def detailsView(request, id):
    if request.user.is_authenticated:
        teacher = serializers.serialize("json", Teachers.objects.filter(id=id))
        teacher = json.loads(teacher)
        print(teacher);
        return render(request, 'detail.html', {"teacher": teacher[0]['fields']})
    else:
        return redirect('/')


def edit(request, id):
    if request.user.is_authenticated:
        teacher = Teachers.objects.get(id=id)
        form = TeacherForm(request.POST or None, instance=teacher)
        print(form.is_valid())

        if form.is_valid():
            form.save()
            return redirect('/teachers/list')

        return render(request, 'edit_teacher.html', {"id": id, 'form': form})
    else:
        return redirect('/')


def teacherDetails(request):
    if request.user.is_authenticated:
        teacherId = request.POST['id']
        teacher = serializers.serialize("json", Teachers.objects.filter(id=teacherId))
        teacher = json.loads(teacher)
        print(teacher)
        return JsonResponse({
            "status": True,
            "message": teacher[0]['fields']
        })
    else:
        return redirect('/')


def teacherDelete(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            teacherId = request.POST['teacherId']
            teacher = Teachers.objects.get(id=teacherId)
            teacher.delete()
            print(teacherId)
            return JsonResponse({
                "status": True,
                "message": ""
            })
    else:
        return redirect('/')
