from django.db.models.expressions import Exists
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect

from .forms import uploadFileForms
from crud.models import Teachers

import pandas as pd
import sqlite3

# Create your views here.
def uploadCsvFile(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = uploadFileForms(request.POST, request.FILES)

            if form.is_valid():
                handle_uploaded_file(request.FILES['file'])
                return HttpResponse("File Uploaded succesfully")
            else:
                return HttpResponse("File not Uploaded")
        else:
            form = uploadFileForms()
            return render(request, 'index.html', {'form': form})
    else:
        return redirect('/')

def handle_uploaded_file(f):
    import sys
    if sys.version_info[0] < 3: 
        from StringIO import StringIO
    else:
        from io import StringIO

    for chunk in f.chunks():
        TESTDATA = StringIO(chunk.decode('utf-8'))
        df = pd.read_csv(TESTDATA, sep=",")
        df = df.dropna()
    from crud.models import Teachers
    subjectList = []
    for x in df['Subjects taught']:
        for subject in Teachers.subject_choices:
            x = x.lower().replace(subject[1].lower(), str(subject[0]))

        subjectList.append(x)
    df['Subjects taught'] = subjectList
    headerList = []
    for headers in list(df.columns.values):
        headers = headers.lower().replace(' ', '_')
        if headers == 'email_address':
            headers = 'email'
        elif headers == 'subjects_taught':
            headers = 'subjects'
        headerList.append(headers)
    
    df.columns = headerList

    con = sqlite3.connect("db.sqlite3")
    try:
        df.to_sql(name='crud_teachers', con=con, if_exists='append', index=False)
    except:
        pass
