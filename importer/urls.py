from django.urls import path
from . import views

urlpatterns = [
    path('csv', views.uploadCsvFile, name='csv-file-upload'),
]
